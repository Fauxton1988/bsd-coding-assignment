This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Background

This project was assigned by Black Swan Data to assess my coding abilities, coding styles, design, and speed of implementation.
At it's most basic it is an interface for interacting with various github apis.

## Considerations

Please note that I used my 'Invictus Capital' Assignment as boilerplate. This is purely for setup and is also the general structure I follow.
All actual logic, designs and layout are unique to the BSD assignement.

### Components

For this particular solution I utilised material-ui purely to focus on the task at hand and not to get to bogged down on
perfectly styling my own custom UI elements.

### State Management

Although not necessary for such a small project, I ended up using Redux Toolkit, which is an excellent tool for
abstracting away a lot of the boiler traditionally required by redux.

### Project Structure

Redux Toolkit required that state management logic utilise the 'ducks' pattern. All traditional aspects of redux
(actions, reducers, action creators) are now hosted in what is know as a slice. Major views are organised into features.
Features also allow for easy reuse among different projects, since we only need to import the feature folder and hook up
the already existing reducer.
Only truly reusable (ie can be reused by different features) components are added to the common folder.

### Code Quality

To keep my formatting and coding standards consistent, I opted to use prettier and eslint. I've also set up a pre-commit
hook to ensure that we do not commit poorly formatted code to the remote repo.

## Challenges

### Interface Design

It was quite a challenge to utilise the screen real estate effectively, especially for repository overviews. I would
have liked to do a better design, even though the current design does its job of conveying the information.

## Future Considerations

### Designs

In future some of the designs could be better thought out and be a bit more aesthetically pleasing.

### Testing

A much greater test coverage should be achieved for the project, especially focussing on state management and testing
out reducers.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser (Should open up automatically).

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
