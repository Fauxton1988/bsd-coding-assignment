import React from 'react';
import { Switch, Route, BrowserRouter as Router } from 'react-router-dom';
import './App.css';
import { Header } from '../common/Header/Header';
import { Search } from '../features/search/Search';
import { Repository } from '../features/repository/Respository';

function App() {
    return (
        <div className="App">
            <Router>
                <Header />
                <Switch>
                    <Route exact path="/">
                        <Search data-testid="search-component" />
                    </Route>
                    <Route exact path="/repository">
                        <Repository data-testid="repository-component" />
                    </Route>
                </Switch>
            </Router>
        </div>
    );
}

export default App;
