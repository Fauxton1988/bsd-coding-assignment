import { combineReducers } from '@reduxjs/toolkit';
import searchReducer from '../features/search/searchSlice';
import repositoryReducer from '../features/repository/respositorySlice';

const rootReducer = combineReducers({
    search: searchReducer,
    repo: repositoryReducer,
});

export default rootReducer;
