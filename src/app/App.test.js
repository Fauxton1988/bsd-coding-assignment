import React from 'react';
import { render } from '@testing-library/react';
import { queryByTestId } from '@testing-library/dom';
import '@testing-library/jest-dom/extend-expect';
import App from './App';
import { initialState } from '../test/initialState.js';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

const defaultProps = {};

const setup = (props) => {
    const mockStore = configureStore();
    const setupProps = { ...defaultProps, ...props };
    const store = mockStore(initialState);
    return render(
        <Provider store={store}>
            <App />
        </Provider>
    );
};

describe('App Test Suite', () => {
    test('App header renders correctly', () => {
        const { getByText, container } = setup();
        const button = queryByTestId(container, 'search-button');

        expect(button).toBeInTheDocument();
        expect(getByText('GitNest')).not.toBeNull();
    });
});
