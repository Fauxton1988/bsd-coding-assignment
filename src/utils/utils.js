export const formatDateTime = (dateTimeString) => {
    return new Date(dateTimeString).toLocaleDateString('en-gb', {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric',
    });
};

export const getOpenIssues = (issues) => {
    return issues.filter((issue) => issue.state === 'open');
};
export const getClosedIssues = (issues) => {
    return issues.filter((issue) => issue.state === 'closed');
};
