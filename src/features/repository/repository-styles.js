import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    issues: {
        width: '100%',
        padding: theme.spacing(3),
    },
    tabPanel: {
        display: 'flex',
        justifyContent: 'center',
    },
    issueColumn: {
        width: '50%',
    },
}));
