import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    issues: {
        padding: theme.spacing(3),
    },
    list: {
        border: '1px solid rgba(0, 0, 0, 0.12)',
        borderRadius: '4px',
        marginTop: '8px',
        maxHeight: '509px',
        overflowY: 'scroll',
        backgroundColor: 'white',
    },
    statsContainer: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: '8px',
    },
    statsPaper: {
        padding: '16px 0',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-star',
        alignItems: 'center',
        width: '100%',
    },
    statsTitle: {
        fontSize: '1.5em',
        padding: theme.spacing(2),
    },
    statsText: {
        paddingTop: theme.spacing(2),
    },
    '@global': {
        '*::-webkit-scrollbar': {
            width: '1px',
        },
        '*::-webkit-scrollbar-track': {
            '-webkit-box-shadow': 'inset 0 0 6px rgba(0,0,0,0.00)',
        },
        '*::-webkit-scrollbar-thumb': {
            backgroundColor: 'rgba(63, 81, 181, 1)',
            outline: '1px solid rgba(63, 81, 181, 1)',
            borderRadius: '5px',
        },
    },
}));
