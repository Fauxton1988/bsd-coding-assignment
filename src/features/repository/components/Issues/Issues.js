import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import Grid from '@material-ui/core/Grid';

import {
    FormControlLabel,
    FormLabel,
    List,
    ListItem,
    ListItemText,
    Radio,
    RadioGroup,
    Typography,
    Paper,
} from '@material-ui/core';
import { Loader } from '../../../../common/Loader/Loader';

import { useStyles } from './IssuesStyles';
import IssueItem from './IssueItem';

import { getOpenIssues, getClosedIssues } from '../../../../utils/utils';
import IssuesChart from './IssuesChart';

const Issues = () => {
    const classes = useStyles();
    const issues = useSelector(({ repo }) => repo.issues);
    const status = useSelector(({ repo }) => repo.status);

    const [filteredIssues, setFilteredIssues] = useState([]);

    useEffect(() => {
        setFilteredIssues(issues);
    }, [issues]);

    const handleIssuesClick = (issues, status) => {
        if (status === 'open') {
            setFilteredIssues(getOpenIssues(issues));
        } else if (status === 'closed') {
            setFilteredIssues(getClosedIssues(issues));
        } else {
            setFilteredIssues(issues);
        }
    };

    const renderRadioBtn = (status) => {
        return (
            <FormControlLabel
                value={status}
                control={<Radio color="primary" />}
                label={status}
                labelPlacement="start"
                onClick={() => handleIssuesClick(issues, status)}
            />
        );
    };

    return (
        <Grid container className={classes.issues}>
            <Grid container item alignItems="center">
                <FormLabel component="legend">Filter By Status:</FormLabel>
                <RadioGroup
                    row
                    aria-label="position"
                    name="position"
                    defaultValue="all"
                >
                    {renderRadioBtn('all')}
                    {renderRadioBtn('closed')}
                    {renderRadioBtn('open')}
                </RadioGroup>
            </Grid>
            {status !== 'loading' ? (
                <Grid container item direction="row" spacing={3}>
                    <Grid item lg={6} xs={12}>
                        <List className={classes.list}>
                            {filteredIssues.length ? (
                                filteredIssues.map((issue) => (
                                    <IssueItem issue={issue} key={issue.id} />
                                ))
                            ) : (
                                <ListItem alignItems="flex-start">
                                    <ListItemText primary="No Issues Found" />
                                </ListItem>
                            )}
                        </List>
                    </Grid>
                    <Grid
                        container
                        item
                        lg={6}
                        xs={12}
                        className={classes.statsContainer}
                    >
                        <Paper
                            square
                            variant="outlined"
                            className={classes.statsPaper}
                        >
                            <Typography className={classes.statsTitle}>
                                Issue Stats
                            </Typography>
                            <div className={{ width: '50%' }}>
                                <IssuesChart issues={issues} />
                            </div>
                            <Typography className={classes.statsText}>
                                Total Issues: {issues.length}
                            </Typography>
                            <Typography className={classes.statsText}>
                                Total Open Issues:{' '}
                                {getOpenIssues(issues).length}
                            </Typography>
                            <Typography className={classes.statsText}>
                                Total Closed Issues:{' '}
                                {getClosedIssues(issues).length}
                            </Typography>
                        </Paper>
                    </Grid>
                </Grid>
            ) : (
                <Loader status={status} />
            )}
        </Grid>
    );
};

export default Issues;
