import React from 'react';
import PropTypes from 'prop-types';
import { PieChart } from 'react-minimal-pie-chart';
import { getClosedIssues, getOpenIssues } from '../../../../utils/utils';

const IssuesChart = ({ issues }) => {
    return (
        <PieChart
            data={[
                {
                    title: 'Open',
                    value: getOpenIssues(issues).length,
                    color: 'rgba(63, 81, 181, 1)',
                },
                {
                    title: 'Closed',
                    value: getClosedIssues(issues).length,
                    color: 'rgba(63, 81, 181, 0.1)',
                },
            ]}
            label={({ dataEntry }) => `${dataEntry.value} (${dataEntry.title})`}
            labelStyle={{
                fontSize: '4px',
                fontFamily: 'sans-serif',
            }}
        />
    );
};

IssuesChart.propTypes = {
    issues: PropTypes.array,
};

export default IssuesChart;
