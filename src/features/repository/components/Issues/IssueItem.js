import React from 'react';
import PropTypes from 'prop-types';
import {
    IconButton,
    ListItem,
    ListItemSecondaryAction,
    ListItemText,
} from '@material-ui/core';
import { Link } from '@material-ui/icons';
import Divider from '@material-ui/core/Divider';

const IssueItem = ({ issue }) => {
    return (
        <div key={issue.id}>
            <ListItem alignItems="flex-start">
                <ListItemText
                    primary={`${issue.title} (${issue.state})`}
                    secondary={`created by ${issue.user.login}`}
                />
                <ListItemSecondaryAction>
                    <IconButton
                        onClick={() => window.open(issue.html_url, '_blank')}
                    >
                        <Link color="primary" />
                    </IconButton>
                </ListItemSecondaryAction>
            </ListItem>
            <Divider component="li" />
        </div>
    );
};

IssueItem.propTypes = {
    issue: PropTypes.shape({
        id: PropTypes.number,
        title: PropTypes.string,
        state: PropTypes.string,
        user: PropTypes.shape({
            login: PropTypes.string,
        }),
        html_url: PropTypes.string,
    }),
};

export default IssueItem;
