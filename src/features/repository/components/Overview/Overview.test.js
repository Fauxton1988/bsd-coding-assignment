import React from 'react';
import { render } from '@testing-library/react';
import { queryByTestId } from '@testing-library/dom';
import '@testing-library/jest-dom/extend-expect';
import Overview from './Overview';
import { initialState } from '../../../../test/initialState.js';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

const setup = () => {
    const mockStore = configureStore();
    const store = mockStore(initialState);
    console.log(store);
    return render(
        <Provider store={store}>
            <Overview />
        </Provider>
    );
};

describe('Search Test Suite', () => {
    let container;

    beforeEach(() => {
        container = setup().container;
    });

    test('Overview renders without error', () => {
        const overview = queryByTestId(container, 'repo-overview');

        expect(overview).toBeInTheDocument();
    });

    test('Rendered Overview View matches expected state', () => {
        const title = queryByTestId(container, 'repo-title');
        const forks = queryByTestId(container, 'repo-forks');

        // TODO Fix test
    });
});
