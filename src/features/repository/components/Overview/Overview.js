import React from 'react';
import { useSelector } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import StatCard from './StatCard';
import RepoUrl from './RepoUrl';
import { formatDateTime } from '../../../../utils/utils';

import { useStyles } from './OverviewStyle';

const Overview = () => {
    const classes = useStyles();
    const repo = useSelector(({ repo }) => repo.repo);

    return (
        repo && (
            <Grid
                data-testid="repo-overview"
                container
                className={classes.overview}
                direction="column"
                alignItems="center"
            >
                <Grid container item justify="center">
                    <div className={classes.overviewHeader}>
                        <a
                            data-testid="repo-title"
                            className={classes.title}
                            href={repo.html_url}
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            {repo.full_name}
                        </a>
                        <span className={classes.overviewDescription}>
                            {repo.description}
                        </span>
                    </div>
                </Grid>
                <Grid container item spacing={3} className={classes.gridCenter}>
                    <StatCard
                        testid="repo-forks"
                        title="Forks"
                        value={String(repo.forks)}
                    />
                    <StatCard
                        testid="repo-open-issues"
                        title="Open Issues"
                        value={String(repo.open_issues)}
                    />
                    <StatCard
                        testid="repo-stargazers"
                        title="Stargazers"
                        value={String(repo.stargazers_count)}
                    />
                    <StatCard
                        testid="repo-created"
                        title="Created"
                        value={formatDateTime(repo.created_at)}
                    />
                    <StatCard
                        testid="repo-updated"
                        title="Updated"
                        value={formatDateTime(repo.updated_at)}
                    />
                </Grid>
                <Grid
                    container
                    item
                    className={classes.urlContainer}
                    justify="center"
                    spacing={3}
                >
                    <div className={classes.subTitle}>Important Urls:</div>
                    <RepoUrl title="Github" value={repo.html_url} />
                    <RepoUrl title="Homepage" value={repo.homepage} />
                    <RepoUrl title="SSH clone" value={repo.ssh_url} />
                    <RepoUrl title="HTTPS clone" value={repo.clone_url} />
                </Grid>
            </Grid>
        )
    );
};

export default Overview;
