import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    paper: {
        borderRadius: 0,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        padding: theme.spacing(3),
    },
    paperTitle: {
        fontSize: '1.2em',
        fontWeight: '300',
    },
    paperValue: {
        fontSize: '1.2em',
        fontWeight: '700',
        textAlign: 'center',
    },
}));

const StatCard = ({ title, value, testid }) => {
    const classes = useStyles();
    return (
        <Grid item lg={4} xs={12}>
            <Paper className={classes.paper}>
                <Typography className={classes.paperTitle}>
                    {`${title}:`}
                </Typography>
                <Typography data-testid={testid} className={classes.paperValue}>
                    {value}
                </Typography>
            </Paper>
        </Grid>
    );
};

StatCard.propTypes = {
    title: PropTypes.string,
    value: PropTypes.string,
    testid: PropTypes.string,
};

export default StatCard;
