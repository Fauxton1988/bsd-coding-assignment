import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    urlTitle: {
        fontSize: '1em',
        fontWeight: '300',
    },
    urlPath: {
        fontSize: '1em',
        fontWeight: '500',
        textAlign: 'center',
    },
    link: {
        textDecoration: 'none',
        color: 'rgba(63, 81, 181, 1)',
    },
    textarea: {
        width: '300px',
    },
}));

const isClickableLink = (title) => {
    return title !== 'SSH clone' && title !== 'HTTPS clone';
};

const RepoUrl = ({ title, value }) => {
    const classes = useStyles();
    return value ? (
        <Grid container item justify="center">
            <Typography className={classes.urlTitle}>{title}:&nbsp;</Typography>
            <Typography className={classes.urlPath}>
                {isClickableLink(title) ? (
                    <a className={classes.link} href={value}>
                        {value}
                    </a>
                ) : (
                    <TextareaAutosize
                        className={classes.textarea}
                        rowsMin={1}
                        rowsMax={1}
                        defaultValue={value}
                    />
                )}
            </Typography>
        </Grid>
    ) : null;
};

RepoUrl.propTypes = {
    title: PropTypes.string,
    value: PropTypes.string,
};

export default RepoUrl;
