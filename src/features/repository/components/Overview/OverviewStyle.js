import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    tabPanel: {
        display: 'flex',
        justifyContent: 'center',
    },
    overview: {
        padding: theme.spacing(3),
        width: '80%',
    },
    gridCenter: {
        display: 'flex',
        justifyContent: 'center',
        marginTop: theme.spacing(4),
    },
    overviewHeader: {
        display: 'flex',
        flexDirection: 'column',
    },
    statsPaper: {
        padding: '16px',
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
    },
    title: {
        fontSize: '26px',
        alignSelf: 'center',
        textDecoration: 'none',
        color: 'rgba(63, 81, 181, 1)',
    },
    subTitle: {
        fontSize: '22px',
    },
    overviewDescription: {
        textAlign: 'center',
        fontSize: '18px',
    },
    urlContainer: {
        paddingTop: theme.spacing(4),
    },
}));
