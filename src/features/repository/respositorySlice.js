import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import api from '../../api';

const repoInitialState = {
    repo: JSON.parse(localStorage.getItem('repo')) || null,
    issues: [],
    status: 'idle',
    error: null,
};

export const fetchIssues = createAsyncThunk(
    'repo/fetchIssues',
    async ({ user, repo, status }) => {
        const repositories = await api.getIssuesByStatus(user, repo, status);

        return repositories.data;
    }
);

const repositorySlice = createSlice({
    name: 'repo',
    initialState: repoInitialState,
    reducers: {
        statusToIdle: (state) => {
            state.status = 'idle';
        },
        selectRepo: (state, { payload }) => {
            localStorage.setItem('repo', JSON.stringify(payload));
            state.repo = payload;
        },
    },
    extraReducers: {
        [fetchIssues.fulfilled]: (state, { payload }) => {
            state.status = 'succeeded';
            state.issues = payload;
        },
        [fetchIssues.pending]: (state) => {
            state.status = 'loading';
        },
        [fetchIssues.rejected]: (state, action) => {
            state.status = 'failed';
            state.error = action.error.message;
        },
    },
});

export const {
    statusToIdle,
    selectRepo,
    updateIssues,
} = repositorySlice.actions;

export default repositorySlice.reducer;
