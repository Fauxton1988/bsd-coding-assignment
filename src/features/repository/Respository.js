import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { fetchIssues } from './respositorySlice';
import { useStyles } from './repository-styles';

import Overview from './components/Overview/Overview';
import Issues from './components/Issues/Issues';

const TabPanel = (props) => {
    const { children, value, index, ...other } = props;
    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && children}
        </div>
    );
};

TabPanel.propTypes = {
    children: PropTypes.element,
    value: PropTypes.number,
    index: PropTypes.number,
};

const a11yProps = (index) => {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
};

export const Repository = () => {
    const dispatch = useDispatch();
    const classes = useStyles();

    const repo = useSelector(({ repo }) => repo.repo);
    const error = useSelector(({ repo }) => repo.error);

    const [value, setValue] = useState(0);

    const handleIssuesClick = () => {
        dispatch(
            fetchIssues({
                user: repo.owner.login,
                repo: repo.name,
                status: 'all',
            })
        );
    };

    return !error ? (
        <div data-testid="repo-tabs">
            <Paper>
                <Tabs
                    value={value}
                    indicatorColor="primary"
                    textColor="primary"
                    onChange={(event, newValue) => setValue(newValue)}
                >
                    <Tab label="Overview" {...a11yProps(0)} />
                    <Tab
                        label="Issues"
                        {...a11yProps(1)}
                        onClick={handleIssuesClick}
                    />
                </Tabs>
            </Paper>
            <TabPanel className={classes.tabPanel} value={value} index={0}>
                <Overview />
            </TabPanel>
            <TabPanel className={classes.tabPanel} value={value} index={1}>
                <Issues />
            </TabPanel>
        </div>
    ) : (
        <div data-testid="repo-tabs">{error}</div>
    );
};
