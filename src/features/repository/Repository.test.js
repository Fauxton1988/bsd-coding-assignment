import React from 'react';
import { render } from '@testing-library/react';
import { queryByTestId } from '@testing-library/dom';
import '@testing-library/jest-dom/extend-expect';
import { Repository } from './Respository';
import { initialState } from '../../test/initialState.js';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

const setup = () => {
    const mockStore = configureStore();
    const store = mockStore(initialState);
    return render(
        <Provider store={store}>
            <Repository />
        </Provider>
    );
};

describe('Repository Test Suite', () => {
    let container;

    beforeEach(() => {
        container = setup().container;
    });

    test('Repo renders correctly', () => {
        const repoTabs = queryByTestId(container, 'repo-tabs');

        expect(repoTabs).toBeInTheDocument();
    });
});
