import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { queryByTestId } from '@testing-library/dom';
import '@testing-library/jest-dom/extend-expect';
import { Search } from './Search';
import { initialState } from '../../test/initialState.js';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

const setup = () => {
    const mockStore = configureStore();
    const store = mockStore(initialState);
    return render(
        <Provider store={store}>
            <Search />
        </Provider>
    );
};

describe('Search Test Suite', () => {
    let container;

    beforeEach(() => {
        container = setup().container;
    });

    test('Searchbar renders correctly', () => {
        const searchBar = queryByTestId(container, 'search-bar');

        expect(searchBar).toBeInTheDocument();
    });

    test('Searchbar value updates correctly', () => {
        const searchTerm = 'tetris';
        const searchBar = queryByTestId(container, 'search-bar');

        fireEvent.change(searchBar.firstChild, {
            target: { value: searchTerm },
        });

        expect(searchTerm).toEqual(searchBar.firstChild.value);
    });
});
