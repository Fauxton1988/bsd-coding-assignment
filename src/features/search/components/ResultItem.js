import React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import PropTypes from 'prop-types';
import ListItem from '@material-ui/core/ListItem';
import { selectRepo } from '../../repository/respositorySlice';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) =>
    createStyles({
        listItem: {
            '&:hover': {
                backgroundColor: 'rgba(63, 81, 181, 0.1)',
                cursor: 'pointer',
            },
        },
    })
);

const ResultItem = ({ repo }) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const history = useHistory();

    return (
        <div key={repo.id}>
            <ListItem
                onClick={() => {
                    dispatch(selectRepo(repo));
                    history.push('/repository');
                }}
                className={classes.listItem}
                alignItems="flex-start"
            >
                <ListItemText
                    primary={repo.full_name}
                    secondary={repo.description}
                />
            </ListItem>
            <Divider component="li" />
        </div>
    );
};

ResultItem.propTypes = {
    repo: PropTypes.shape({
        id: PropTypes.number,
        full_name: PropTypes.string,
        description: PropTypes.string,
    }),
};

export default ResultItem;
