import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) =>
    createStyles({
        container: {
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            marginTop: theme.spacing(2),
        },
        search: {
            width: '80%',
        },
        list: {
            width: '80%',
            border: '1px solid rgba(0, 0, 0, 0.12)',
            borderRadius: '4px',
            marginTop: '8px',
            maxHeight: '500px',
            overflowY: 'scroll',
        },
        noResult: {
            display: 'flex',
            justifyContent: 'center',
        },
        '@global': {
            '*::-webkit-scrollbar': {
                width: '1px',
            },
            '*::-webkit-scrollbar-track': {
                '-webkit-box-shadow': 'inset 0 0 6px rgba(0,0,0,0.00)',
            },
            '*::-webkit-scrollbar-thumb': {
                backgroundColor: 'rgba(63, 81, 181, 1)',
                outline: '1px solid rgba(63, 81, 181, 1)',
                borderRadius: '5px',
            },
        },
    })
);
