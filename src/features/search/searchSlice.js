import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import api from '../../api';

const searchInitialState = {
    searchResults: null,
    status: 'idle',
    error: null,
};

export const performSearch = createAsyncThunk(
    'search/performSearch',
    async (name) => {
        const repositories = await api.getRepoByName(name);

        return repositories.data.items;
    }
);

const searchSlice = createSlice({
    name: 'search',
    initialState: searchInitialState,
    reducers: {
        statusToIdle: (state) => {
            state.status = 'idle';
        },
    },
    extraReducers: {
        [performSearch.fulfilled]: (state, { payload }) => {
            state.status = 'succeeded';
            state.searchResults = payload;
        },
        [performSearch.pending]: (state) => {
            state.status = 'loading';
        },
        [performSearch.rejected]: (state, action) => {
            state.status = 'failed';
            state.error = action.error.message;
        },
    },
});

export const { statusToIdle } = searchSlice.actions;

export default searchSlice.reducer;
