import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import SearchBar from 'material-ui-search-bar';
import List from '@material-ui/core/List';
import { Loader } from '../../common/Loader/Loader';
import ResultItem from './components/ResultItem';

import { performSearch } from './searchSlice';

import { useStyles } from './SearchStyles';

export const Search = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [searchTerm, setSearchTerm] = useState('');

    const repositories = useSelector(({ search }) => search.searchResults);
    const status = useSelector(({ search }) => search.status);

    return (
        <div className={classes.container}>
            <SearchBar
                data-testid="search-bar"
                className={classes.search}
                value={searchTerm}
                onChange={setSearchTerm}
                onRequestSearch={() => dispatch(performSearch(searchTerm))}
            />
            <Loader status={status} />
            {repositories && (
                <List className={classes.list} data-testid="repo-list">
                    {repositories.length ? (
                        repositories.map((repo) => {
                            return <ResultItem repo={repo} key={repo.id} />;
                        })
                    ) : (
                        <div className={classes.noResult}>
                            No Repositories Found
                        </div>
                    )}
                </List>
            )}
        </div>
    );
};
