import React from 'react';
import PropTypes from 'prop-types';
import Spinner from 'react-spinners/BeatLoader';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) =>
    createStyles({
        spinner: {
            display: 'flex',
            justifyContent: 'center',
        },
    })
);

export const Loader = ({ status }) => {
    const classes = useStyles();

    return (
        <Spinner
            className={classes.spinner}
            size={10}
            color={'#3f51b5'}
            loading={status === 'loading'}
        />
    );
};

Loader.propTypes = {
    status: PropTypes.string,
};
