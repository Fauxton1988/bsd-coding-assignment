import React from 'react';
import { useHistory } from 'react-router';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { AppBar, Toolbar, Typography, Button } from '@material-ui/core';
import { GitHub } from '@material-ui/icons';

const useStyles = makeStyles((theme) =>
    createStyles({
        toolbar: {
            display: 'flex',
            justifyContent: 'space-between',
        },
        titleContainer: {
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
        },
        title: {
            paddingLeft: theme.spacing(1),
        },
        button: {
            color: 'white',
        },
        '&:hover': {
            color: {},
        },
    })
);

export const Header = () => {
    const history = useHistory();
    const classes = useStyles();

    return (
        <div>
            <AppBar position="static">
                <Toolbar className={classes.toolbar}>
                    <div className={classes.titleContainer}>
                        <GitHub />
                        <Typography className={classes.title} variant="h6">
                            GitNest
                        </Typography>
                    </div>
                    <Button
                        className={classes.button}
                        onClick={() => history.push('/')}
                        data-testid="search-button"
                    >
                        Change Repo
                    </Button>
                </Toolbar>
            </AppBar>
        </div>
    );
};
