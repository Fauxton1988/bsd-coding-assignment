import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://api.github.com',
    headers: {
        'content-type': 'application/json',
    },
});

instance.interceptors.request.use(
    (response) => {
        return response;
    },
    (error) => {
        return Promise.reject(error);
    }
);

const get = (url) => instance({ method: 'GET', url: `${url}` });

export default {
    getRepoByName: (name) => get(`/search/repositories?q=${encodeURI(name)}`),
    getIssuesByStatus: (user, repo, status) =>
        get(
            `/repos/${encodeURI(user)}/${encodeURI(
                repo
            )}/issues?state=${encodeURI(status)}`
        ),
};
